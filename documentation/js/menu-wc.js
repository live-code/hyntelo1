'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">hyntelo-1 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccordionDemoModule.html" data-type="entity-link" >AccordionDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AccordionDemoModule-870c62d859ccbb60f523035062abfdc634b5fa5348ee3440794a52d6b4491cea084936fa4c87c675f7d922f21454b84fe80be9b5c59bc056801b288f3e6de625"' : 'data-target="#xs-components-links-module-AccordionDemoModule-870c62d859ccbb60f523035062abfdc634b5fa5348ee3440794a52d6b4491cea084936fa4c87c675f7d922f21454b84fe80be9b5c59bc056801b288f3e6de625"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AccordionDemoModule-870c62d859ccbb60f523035062abfdc634b5fa5348ee3440794a52d6b4491cea084936fa4c87c675f7d922f21454b84fe80be9b5c59bc056801b288f3e6de625"' :
                                            'id="xs-components-links-module-AccordionDemoModule-870c62d859ccbb60f523035062abfdc634b5fa5348ee3440794a52d6b4491cea084936fa4c87c675f7d922f21454b84fe80be9b5c59bc056801b288f3e6de625"' }>
                                            <li class="link">
                                                <a href="components/AccordionDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AccordionDemoRoutingModule.html" data-type="entity-link" >AccordionDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-4eed45c4630a65afeddf56f5ed619a80d7c2c28131fb071fe94caf9be9512c9546a561b75229b4f51147862c3ae09d1e5a99d82e6ac3871fc8d4fed54952d5d6"' : 'data-target="#xs-components-links-module-AppModule-4eed45c4630a65afeddf56f5ed619a80d7c2c28131fb071fe94caf9be9512c9546a561b75229b4f51147862c3ae09d1e5a99d82e6ac3871fc8d4fed54952d5d6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-4eed45c4630a65afeddf56f5ed619a80d7c2c28131fb071fe94caf9be9512c9546a561b75229b4f51147862c3ae09d1e5a99d82e6ac3871fc8d4fed54952d5d6"' :
                                            'id="xs-components-links-module-AppModule-4eed45c4630a65afeddf56f5ed619a80d7c2c28131fb071fe94caf9be9512c9546a561b75229b4f51147862c3ae09d1e5a99d82e6ac3871fc8d4fed54952d5d6"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContactsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonsDemoModule.html" data-type="entity-link" >ButtonsDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ButtonsDemoModule-6a6b0f38a84be6496917eea00c257e34aa185894efdae9cf7e10167a0a1389205d0272a905da684a658fb7384b7a795d817384d741cf2c00530f6225a79121c6"' : 'data-target="#xs-components-links-module-ButtonsDemoModule-6a6b0f38a84be6496917eea00c257e34aa185894efdae9cf7e10167a0a1389205d0272a905da684a658fb7384b7a795d817384d741cf2c00530f6225a79121c6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ButtonsDemoModule-6a6b0f38a84be6496917eea00c257e34aa185894efdae9cf7e10167a0a1389205d0272a905da684a658fb7384b7a795d817384d741cf2c00530f6225a79121c6"' :
                                            'id="xs-components-links-module-ButtonsDemoModule-6a6b0f38a84be6496917eea00c257e34aa185894efdae9cf7e10167a0a1389205d0272a905da684a658fb7384b7a795d817384d741cf2c00530f6225a79121c6"' }>
                                            <li class="link">
                                                <a href="components/ButtonsDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonsDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonsDemoRoutingModule.html" data-type="entity-link" >ButtonsDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CardModule.html" data-type="entity-link" >CardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CardModule-b3404c0891dd3bd7602b0a8d7af2b5fc16b3a0b1d4ee7738d40925d6d636e0181b8ba5c1a0bb7f98b40a33d6eaa03d1bbb65837b8f7dda1733145738f64832c2"' : 'data-target="#xs-components-links-module-CardModule-b3404c0891dd3bd7602b0a8d7af2b5fc16b3a0b1d4ee7738d40925d6d636e0181b8ba5c1a0bb7f98b40a33d6eaa03d1bbb65837b8f7dda1733145738f64832c2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CardModule-b3404c0891dd3bd7602b0a8d7af2b5fc16b3a0b1d4ee7738d40925d6d636e0181b8ba5c1a0bb7f98b40a33d6eaa03d1bbb65837b8f7dda1733145738f64832c2"' :
                                            'id="xs-components-links-module-CardModule-b3404c0891dd3bd7602b0a8d7af2b5fc16b3a0b1d4ee7738d40925d6d636e0181b8ba5c1a0bb7f98b40a33d6eaa03d1bbb65837b8f7dda1733145738f64832c2"' }>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogModule.html" data-type="entity-link" >CatalogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' : 'data-target="#xs-components-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' :
                                            'id="xs-components-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' }>
                                            <li class="link">
                                                <a href="components/CatalogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' : 'data-target="#xs-injectables-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' :
                                        'id="xs-injectables-links-module-CatalogModule-7a9a6ba190d21e2c16a7eb9816b50be0217eca67576ac80de3844bba0d3bb7dd6680e60c23128ec8742a21714839ed3b6e22cdf4eee2a7d19211f059a532bdea"' }>
                                        <li class="link">
                                            <a href="injectables/CatalogActionsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogActionsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/CatalogStoreService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogStoreService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChartDemoModule.html" data-type="entity-link" >ChartDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChartDemoModule-c62e4ba532a1e52498f534f33c8186b12fc24f464b4f820755506c86c19a95dd90f9edd35dbd7eba7410ed4f7e66c521e4187a237712efd6d65ddd770979076a"' : 'data-target="#xs-components-links-module-ChartDemoModule-c62e4ba532a1e52498f534f33c8186b12fc24f464b4f820755506c86c19a95dd90f9edd35dbd7eba7410ed4f7e66c521e4187a237712efd6d65ddd770979076a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChartDemoModule-c62e4ba532a1e52498f534f33c8186b12fc24f464b4f820755506c86c19a95dd90f9edd35dbd7eba7410ed4f7e66c521e4187a237712efd6d65ddd770979076a"' :
                                            'id="xs-components-links-module-ChartDemoModule-c62e4ba532a1e52498f534f33c8186b12fc24f464b4f820755506c86c19a95dd90f9edd35dbd7eba7410ed4f7e66c521e4187a237712efd6d65ddd770979076a"' }>
                                            <li class="link">
                                                <a href="components/ChartDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChartDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChartDemoRoutingModule.html" data-type="entity-link" >ChartDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-e88f0cc7028d336c81dd5e0ca85a8e4f241b02c4ee7590ade5ab9aa547e8b55c8f6150cda0bc3cba8831544a36db66f128778c643334c69ba49d4347645b5451"' : 'data-target="#xs-components-links-module-CoreModule-e88f0cc7028d336c81dd5e0ca85a8e4f241b02c4ee7590ade5ab9aa547e8b55c8f6150cda0bc3cba8831544a36db66f128778c643334c69ba49d4347645b5451"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-e88f0cc7028d336c81dd5e0ca85a8e4f241b02c4ee7590ade5ab9aa547e8b55c8f6150cda0bc3cba8831544a36db66f128778c643334c69ba49d4347645b5451"' :
                                            'id="xs-components-links-module-CoreModule-e88f0cc7028d336c81dd5e0ca85a8e4f241b02c4ee7590ade5ab9aa547e8b55c8f6150cda0bc3cba8831544a36db66f128778c643334c69ba49d4347645b5451"' }>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DynamicDemoModule.html" data-type="entity-link" >DynamicDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DynamicDemoModule-e2fc526a9f18a0067c68608bdbf56c85a29e8313ec7c00bd4f124dacf1d293ef3a46ff9a2c9a026c0d524dbf0db51ab07af4f6dcfa70839f7e35f7a3763be5a7"' : 'data-target="#xs-components-links-module-DynamicDemoModule-e2fc526a9f18a0067c68608bdbf56c85a29e8313ec7c00bd4f124dacf1d293ef3a46ff9a2c9a026c0d524dbf0db51ab07af4f6dcfa70839f7e35f7a3763be5a7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DynamicDemoModule-e2fc526a9f18a0067c68608bdbf56c85a29e8313ec7c00bd4f124dacf1d293ef3a46ff9a2c9a026c0d524dbf0db51ab07af4f6dcfa70839f7e35f7a3763be5a7"' :
                                            'id="xs-components-links-module-DynamicDemoModule-e2fc526a9f18a0067c68608bdbf56c85a29e8313ec7c00bd4f124dacf1d293ef3a46ff9a2c9a026c0d524dbf0db51ab07af4f6dcfa70839f7e35f7a3763be5a7"' }>
                                            <li class="link">
                                                <a href="components/DynamicDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DynamicDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DynamicDemoRoutingModule.html" data-type="entity-link" >DynamicDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-a3707fded6ca0f62d62115f40ca4d89025477da2a06ac950c393534b90a3fdbb5d5a55aed33922d65b3c105224e69a52409c842cc760496a96f67ecc21cab975"' : 'data-target="#xs-components-links-module-LoginModule-a3707fded6ca0f62d62115f40ca4d89025477da2a06ac950c393534b90a3fdbb5d5a55aed33922d65b3c105224e69a52409c842cc760496a96f67ecc21cab975"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-a3707fded6ca0f62d62115f40ca4d89025477da2a06ac950c393534b90a3fdbb5d5a55aed33922d65b3c105224e69a52409c842cc760496a96f67ecc21cab975"' :
                                            'id="xs-components-links-module-LoginModule-a3707fded6ca0f62d62115f40ca4d89025477da2a06ac950c393534b90a3fdbb5d5a55aed33922d65b3c105224e69a52409c842cc760496a96f67ecc21cab975"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostpassComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LostpassComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegistrationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegistrationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SigninComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SigninComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapDemoModule.html" data-type="entity-link" >MapDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MapDemoModule-66fb2c94c0621cc36dc403c9b6d06e6832218a5c92c2df488b464ace501a7d96f9a3b8b4093598bcc824ccef4cac045afa1c26fd7ee05fd971deeda37737f47d"' : 'data-target="#xs-components-links-module-MapDemoModule-66fb2c94c0621cc36dc403c9b6d06e6832218a5c92c2df488b464ace501a7d96f9a3b8b4093598bcc824ccef4cac045afa1c26fd7ee05fd971deeda37737f47d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MapDemoModule-66fb2c94c0621cc36dc403c9b6d06e6832218a5c92c2df488b464ace501a7d96f9a3b8b4093598bcc824ccef4cac045afa1c26fd7ee05fd971deeda37737f47d"' :
                                            'id="xs-components-links-module-MapDemoModule-66fb2c94c0621cc36dc403c9b6d06e6832218a5c92c2df488b464ace501a7d96f9a3b8b4093598bcc824ccef4cac045afa1c26fd7ee05fd971deeda37737f47d"' }>
                                            <li class="link">
                                                <a href="components/MapDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapDemoRoutingModule.html" data-type="entity-link" >MapDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PanelsDemoModule.html" data-type="entity-link" >PanelsDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PanelsDemoModule-dbb3a91599ab54ddb093be6713bc9395d5623d6970bf193d44e512201d7410f8bc6719bc12d73aaa90e4bef814816f55ac32c3c428c844fcc2ec45e0feb4f508"' : 'data-target="#xs-components-links-module-PanelsDemoModule-dbb3a91599ab54ddb093be6713bc9395d5623d6970bf193d44e512201d7410f8bc6719bc12d73aaa90e4bef814816f55ac32c3c428c844fcc2ec45e0feb4f508"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PanelsDemoModule-dbb3a91599ab54ddb093be6713bc9395d5623d6970bf193d44e512201d7410f8bc6719bc12d73aaa90e4bef814816f55ac32c3c428c844fcc2ec45e0feb4f508"' :
                                            'id="xs-components-links-module-PanelsDemoModule-dbb3a91599ab54ddb093be6713bc9395d5623d6970bf193d44e512201d7410f8bc6719bc12d73aaa90e4bef814816f55ac32c3c428c844fcc2ec45e0feb4f508"' }>
                                            <li class="link">
                                                <a href="components/PanelsDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PanelsDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PanelsDemoRoutingModule.html" data-type="entity-link" >PanelsDemoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-812abd027b5dd74bda2e2afb9bb64d2224f3b5514a2da18c12e483b15515f6d3c6b3dabc4d90d5482fd7046a4eea464807098e63d930b12c4a17ea39a3afc915"' : 'data-target="#xs-components-links-module-SharedModule-812abd027b5dd74bda2e2afb9bb64d2224f3b5514a2da18c12e483b15515f6d3c6b3dabc4d90d5482fd7046a4eea464807098e63d930b12c4a17ea39a3afc915"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-812abd027b5dd74bda2e2afb9bb64d2224f3b5514a2da18c12e483b15515f6d3c6b3dabc4d90d5482fd7046a4eea464807098e63d930b12c4a17ea39a3afc915"' :
                                            'id="xs-components-links-module-SharedModule-812abd027b5dd74bda2e2afb9bb64d2224f3b5514a2da18c12e483b15515f6d3c6b3dabc4d90d5482fd7046a4eea464807098e63d930b12c4a17ea39a3afc915"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ColComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ColComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LeafletComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LeafletComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RowComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RowComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ToggableModule.html" data-type="entity-link" >ToggableModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ToggableModule-c074a72311ffc612ae4d3843947730893b833933962cf46ca17132acc90f5752fa1e78bdf0d89c2e29961d90794acb7229a7e18aa2cf4572ceed621ed7ae7562"' : 'data-target="#xs-components-links-module-ToggableModule-c074a72311ffc612ae4d3843947730893b833933962cf46ca17132acc90f5752fa1e78bdf0d89c2e29961d90794acb7229a7e18aa2cf4572ceed621ed7ae7562"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ToggableModule-c074a72311ffc612ae4d3843947730893b833933962cf46ca17132acc90f5752fa1e78bdf0d89c2e29961d90794acb7229a7e18aa2cf4572ceed621ed7ae7562"' :
                                            'id="xs-components-links-module-ToggableModule-c074a72311ffc612ae4d3843947730893b833933962cf46ca17132acc90f5752fa1e78bdf0d89c2e29961d90794acb7229a7e18aa2cf4572ceed621ed7ae7562"' }>
                                            <li class="link">
                                                <a href="components/ToggableComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ToggableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitModule.html" data-type="entity-link" >UikitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitModule-a71b09b32fb11f555dad84f961c9fa0bc3e58c7055d29de28e241d5825dd38a3c8172fe11fee352d336a558f5fd973d60f40450ee5e2be0e6f0ccdaaea5ea017"' : 'data-target="#xs-components-links-module-UikitModule-a71b09b32fb11f555dad84f961c9fa0bc3e58c7055d29de28e241d5825dd38a3c8172fe11fee352d336a558f5fd973d60f40450ee5e2be0e6f0ccdaaea5ea017"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitModule-a71b09b32fb11f555dad84f961c9fa0bc3e58c7055d29de28e241d5825dd38a3c8172fe11fee352d336a558f5fd973d60f40450ee5e2be0e6f0ccdaaea5ea017"' :
                                            'id="xs-components-links-module-UikitModule-a71b09b32fb11f555dad84f961c9fa0bc3e58c7055d29de28e241d5825dd38a3c8172fe11fee352d336a558f5fd973d60f40450ee5e2be0e6f0ccdaaea5ea017"' }>
                                            <li class="link">
                                                <a href="components/UikitComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitRoutingModule.html" data-type="entity-link" >UikitRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/WeatherDemoModule.html" data-type="entity-link" >WeatherDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-WeatherDemoModule-af7e698e741e7dbd8a5f307611dd28f7b81760c782c50eef54bbd30760918a29b9fe93dc6dc5857d9f5ec9af83dadd53ad2d3fe6f715b284aa04f710c7e17f79"' : 'data-target="#xs-components-links-module-WeatherDemoModule-af7e698e741e7dbd8a5f307611dd28f7b81760c782c50eef54bbd30760918a29b9fe93dc6dc5857d9f5ec9af83dadd53ad2d3fe6f715b284aa04f710c7e17f79"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-WeatherDemoModule-af7e698e741e7dbd8a5f307611dd28f7b81760c782c50eef54bbd30760918a29b9fe93dc6dc5857d9f5ec9af83dadd53ad2d3fe6f715b284aa04f710c7e17f79"' :
                                            'id="xs-components-links-module-WeatherDemoModule-af7e698e741e7dbd8a5f307611dd28f7b81760c782c50eef54bbd30760918a29b9fe93dc6dc5857d9f5ec9af83dadd53ad2d3fe6f715b284aa04f710c7e17f79"' }>
                                            <li class="link">
                                                <a href="components/WeatherDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WeatherDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WeatherDemoRoutingModule.html" data-type="entity-link" >WeatherDemoRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/City.html" data-type="entity-link" >City</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Clouds.html" data-type="entity-link" >Clouds</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Coord.html" data-type="entity-link" >Coord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Coords.html" data-type="entity-link" >Coords</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Country.html" data-type="entity-link" >Country</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Main.html" data-type="entity-link" >Main</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Meteo.html" data-type="entity-link" >Meteo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Sys.html" data-type="entity-link" >Sys</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Weather.html" data-type="entity-link" >Weather</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Wind.html" data-type="entity-link" >Wind</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});