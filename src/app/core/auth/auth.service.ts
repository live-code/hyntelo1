import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { Auth, Role } from '../../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient) {
    const authFromLocalstorage = localStorage.getItem('auth')

    if (authFromLocalstorage) {
      this.auth$.next(JSON.parse(authFromLocalstorage))
    }
  }

  login() {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe((res) => {
        this.auth$.next(res);
        localStorage.setItem('auth', JSON.stringify(res))
      })
  }

  logout() {
    this.auth$.next(null)
    localStorage.removeItem('auth')
  }

  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      )
  }

  get role$(): Observable<Role | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      )
  }
  get displayName$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName)
      )
  }
}
