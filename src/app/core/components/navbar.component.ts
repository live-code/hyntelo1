import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'hy-navbar',
  template: `
    <button routerLink="login" routerLinkActive="bg">login</button>
    <button routerLink="home" routerLinkActive="bg">home</button>
    <button routerLink="catalog" routerLinkActive="bg">users (catalog)</button>
    <button routerLink="clients" routerLinkActive="bg">Clients</button>
    <button routerLink="uikit" routerLinkActive="bg">uikit</button>
    <button [routerLink]="'contacts'"  routerLinkActive="bg">contacts</button>
    <button (click)="authService.logout()">Logout</button>
    {{authService.displayName$ | async}}
  `,
  styles: [`
    .bg { background-color: orange}
  `]
})
export class NavbarComponent {
  constructor(public authService: AuthService) {
  }
}
