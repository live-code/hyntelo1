import { Component } from '@angular/core';
import { CatalogStoreService } from './features/catalog/services/catalog-store.service';
import { Page } from './model/page';

@Component({
  selector: 'hy-root',
  template: `
    <hy-navbar></hy-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  label = 'ciao'
  section: Page | null = null;

  changePage(page: Page) {

  }
}
