import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../model/user';
import { CatalogActionsService } from './services/catalog-actions.service';
import { CatalogStoreService } from './services/catalog-store.service';

@Component({
  selector: 'hy-catalog',
  template: `
    <hy-catalog-form
      (save)="actions.save($event)"
    ></hy-catalog-form>
    
    <br>
  
    <hy-catalog-list
      [users]="store.users"
      (deleteUser)="actions.deleteUser($event)"
    ></hy-catalog-list>
  `,
})
export class CatalogComponent {
  constructor(
    public actions: CatalogActionsService,
    public store: CatalogStoreService
  ) {
    actions.getAll()
  }

}
