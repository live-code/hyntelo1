import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CatalogComponent } from './catalog.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogActionsService } from './services/catalog-actions.service';
import { CatalogStoreService } from './services/catalog-store.service';

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent}
    ]),
    SharedModule
  ],
  providers: [
    CatalogActionsService,
    CatalogStoreService,
  ]
})
export class CatalogModule {}
