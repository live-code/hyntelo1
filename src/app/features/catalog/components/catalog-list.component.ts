import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'hy-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li *ngFor="let u of users">
      <input type="text" [ngModel]="u.name">
      <i class="fa fa-trash"
         (click)="deleteUser.emit(u.id)"></i>
    </li>
  `,
})
export class CatalogListComponent  {
  @Input() users: User[] = [];
  @Output() deleteUser = new EventEmitter<number>();
}
