import { ChangeDetectorRef, Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'hy-catalog-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form #f="ngForm" (submit)="save.emit(f)">
      <input type="text" name="name" ngModel required minlength="3">
      <input type="text" name="address" ngModel required>
      <button type="submit"
              [disabled]="f.invalid">ADD</button>
    </form>
  `,
})
export class CatalogFormComponent {
  @Output() save = new EventEmitter<NgForm>();

  constructor(private cd: ChangeDetectorRef) {
   
  }
}
