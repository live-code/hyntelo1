import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../../model/user';
import { CatalogStoreService } from './catalog-store.service';

@Injectable()
export class CatalogActionsService {

  constructor(
    private http: HttpClient,
    private store: CatalogStoreService
  ) {}

  getAll() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe((res) => {
        this.store.init(res)
      })
  }


  save(form: NgForm) {
    this.http.post<User>('http://localhost:3000/users', form.value)
      .subscribe(res => {
        this.store.add(res)
        form.reset();
      })
  }

  deleteUser(idToRemove: number) {
    this.http.delete(`http://localhost:3000/users/${idToRemove}`)
      .subscribe(() => {
        this.store.remove(idToRemove)
      })
  }
}
