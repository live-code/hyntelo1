import { Injectable } from '@angular/core';
import { User } from '../../../model/user';

@Injectable()
export class CatalogStoreService {
  users: User[] = [];

  init(users: User[]) {
    this.users = users;
  }

  add(user: User) {
    // this.users.push(user)
    // this.users = this.users.concat([user])
    this.users = [...this.users, user];
    // const users = JSON.parse(JSON.stringify(this.users))
  }

  remove(id: number) {
    // const index = this.users.findIndex(user => user.id === id);
    // this.users.splice(index, 1)
    this.users = this.users.filter(user => {
      return user.id !== id;
    })
  }
}
