import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartDemoRoutingModule } from './chart-demo-routing.module';
import { ChartDemoComponent } from './chart-demo.component';


@NgModule({
  declarations: [
    ChartDemoComponent
  ],
  imports: [
    CommonModule,
    ChartDemoRoutingModule
  ]
})
export class ChartDemoModule { }
