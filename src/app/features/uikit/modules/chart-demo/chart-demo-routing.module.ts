import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartDemoComponent } from './chart-demo.component';

const routes: Routes = [{ path: '', component: ChartDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartDemoRoutingModule { }
