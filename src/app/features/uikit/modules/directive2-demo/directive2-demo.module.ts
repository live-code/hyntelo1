import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { Directive2DemoRoutingModule } from './directive2-demo-routing.module';
import { Directive2DemoComponent } from './directive2-demo.component';


@NgModule({
  declarations: [
    Directive2DemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    Directive2DemoRoutingModule
  ]
})
export class Directive2DemoModule { }
