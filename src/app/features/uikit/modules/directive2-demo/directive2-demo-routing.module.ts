import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directive2DemoComponent } from './directive2-demo.component';

const routes: Routes = [{ path: '', component: Directive2DemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directive2DemoRoutingModule { }
