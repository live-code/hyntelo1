import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../../../core/auth/auth.service';

@Component({
  selector: 'hy-directive2-demo',
  template: `
    <h1>Structural Directives</h1>
    
    <div *hyIfRoleIs="'admin'">
      features for admins
    </div>
    
    <div *hyIfRoleIs="['admin', 'moderator']">
      features for admins and moderators
    </div>
    
    <div *hyIfLogged>
      sono loggato
    </div>
    
  `,
})
export class Directive2DemoComponent {

}
