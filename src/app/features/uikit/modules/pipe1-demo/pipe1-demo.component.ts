import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { share } from 'rxjs';
import { AuthService } from '../../../../core/auth/auth.service';
import { User } from '../../../../model/user';

@Component({
  selector: 'hy-pipe1-demo',
  template: `
    <input type="number" [(ngModel)]="value">
    <br>
    {{ 1000 | format: 'gb'}}
    <br>
    {{ date | date: 'MMMM'}}
    <br>
    {{ date | timesago}}

    <br>
<!--    <div *ngIf="(1 | user | async) as myUser">
      {{ myUser.id}}
      {{ myUser.name}}
    </div>-->
    {{ (1 | user | async)?.id}}
    {{ (1 | user | async)?.name}}
    
    <hr>
    <input type="text" [(ngModel)]="filterNameValue" placeholder="Name">

    <select [(ngModel)]="filterASCDESC">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>
    </select>


    <li *ngFor="let u of req$ | async 
                | usersSort: filterASCDESC
                | usersSearch: filterNameValue
    ">
      {{u.name}}
    </li>
    
    {{(req$ | async)?.length}}
    
  `,
})
export class Pipe1DemoComponent  {
  filterGenderValue: 'M' | 'F' | 'all' = 'all'
  filterNameValue = ''
  filterASCDESC: 'ASC' | 'DESC' = 'ASC'
  value = 10;
  date = 1642532866

  req$ = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users/')

  constructor(private http: HttpClient) {
    this.req$.subscribe()
  }
}
