import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Pipe1DemoComponent } from './pipe1-demo.component';

const routes: Routes = [{ path: '', component: Pipe1DemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pipe1DemoRoutingModule { }
