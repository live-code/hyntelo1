import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

import { Pipe1DemoRoutingModule } from './pipe1-demo-routing.module';
import { Pipe1DemoComponent } from './pipe1-demo.component';


@NgModule({
  declarations: [
    Pipe1DemoComponent
  ],
  imports: [
    CommonModule,
    Pipe1DemoRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class Pipe1DemoModule { }
