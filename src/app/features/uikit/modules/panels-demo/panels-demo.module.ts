import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { PanelsDemoRoutingModule } from './panels-demo-routing.module';
import { PanelsDemoComponent } from './panels-demo.component';


@NgModule({
  declarations: [
    PanelsDemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PanelsDemoRoutingModule
  ]
})
export class PanelsDemoModule { }
