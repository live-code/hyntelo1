import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-panels-demo',
  template: `
    <div class="container">
      <h1>Panels</h1>

      <pre>{{visible}}</pre>
      <hy-toggable
        [(opened)]="visible"
      >content 1</hy-toggable>
      
      <br>
      
      <hy-card 
        title="Profile" 
        icon="fa fa-google"
        (iconClick)="visible = !visible"
      >
          <input type="text">
          <input type="text">
          <button>SAVE</button>
      </hy-card>
      
      <br>
      <hy-card 
        *ngIf="visible"
        title="Lorem  Ipsum" 
        icon="fa fa-facebook"
        (iconClick)="openLink('http://www.facebook.com')"
      >
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam animi dolorum ducimus ea et itaque minima modi nobis, officia optio praesentium quidem quis quos repellat sint tenetur vel voluptas!  
      </hy-card>
    </div>
  `,
  styles: [
  ]
})
export class PanelsDemoComponent implements OnInit {
  visible = false;

  constructor() { }

  ngOnInit(): void {
  }

  openLink(url: string) {
    window.open(url)
  }
}
