import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-directive1-demo',
  template: `
    <input type="text" [(ngModel)]="color">
    
    <h1>bg</h1>
    <!--<div hyBg [value]="color">one</div>-->
    <div hyBg="cyan">one</div>

    <h1>border</h1>
    <span hyBorder="dotted" color="red">border</span>
    <span hyBorder="dotted" [color]="color">border</span>
   
    <h1>xhr</h1>
    <div
      hyXhr="https://jsonplaceholder.typicode.com/users" 
      (success)="users = $event"
    ></div>
    <div *ngFor="let user of users">{{user.name}}</div>
    
    <h1>Url</h1>
    
    <button hyUrl="http://www.google.com">Google</button>
    
    <div style="background-color: red; padding: 10px" (click)="outer()">
      OUTER
      <div (click)="inner()" hyStopPropagation
        style="background-color: cyan; padding: 10px">
        INNER
      </div>
    </div>
  `,
})
export class Directive1DemoComponent {
  color = 'red'
  users: any[] = []

  inner() {
    console.log('inner')
  }
  outer() {
    console.log('outer')
  }
}
