import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

import { Directive1DemoRoutingModule } from './directive1-demo-routing.module';
import { Directive1DemoComponent } from './directive1-demo.component';


@NgModule({
  declarations: [
    Directive1DemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    Directive1DemoRoutingModule,
    FormsModule
  ]
})
export class Directive1DemoModule { }
