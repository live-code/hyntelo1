import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directive1DemoComponent } from './directive1-demo.component';

const routes: Routes = [{ path: '', component: Directive1DemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directive1DemoRoutingModule { }
