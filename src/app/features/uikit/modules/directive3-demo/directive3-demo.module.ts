import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { Directive3DemoRoutingModule } from './directive3-demo-routing.module';
import { Directive3DemoComponent } from './directive3-demo.component';


@NgModule({
  declarations: [
    Directive3DemoComponent
  ],
  imports: [
    CommonModule,
    Directive3DemoRoutingModule,
    SharedModule
  ]
})
export class Directive3DemoModule { }
