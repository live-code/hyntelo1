import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directive3DemoComponent } from './directive3-demo.component';

const routes: Routes = [{ path: '', component: Directive3DemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directive3DemoRoutingModule { }
