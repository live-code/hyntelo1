import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-directive3-demo',
  template: `
    <p>
      directive3-demo works!
    </p>
    
    <button [hyMap]="[43,13]">Trieste</button>
  `,
  styles: [
  ]
})
export class Directive3DemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
