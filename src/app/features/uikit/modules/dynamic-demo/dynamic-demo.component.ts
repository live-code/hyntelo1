import { Component, ComponentRef, OnInit, TemplateRef, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../../../../shared/components/card.component';
import { LeafletComponent } from '../../../../shared/components/leaflet.component';
import { ToggableComponent } from '../../../../shared/components/toggable.component';
import { WeatherComponent } from '../../../../shared/components/weather.component';

@Component({
  selector: 'hy-dynamic-demo',
  template: `

    <div *ngFor="let w of widgets">
      <div *hyLoeader="w"></div>
    </div>


    <h1>Dynamic Example</h1>

    <div *ngFor="let w of widgets">
      <div [ngSwitch]="w.type">
        <hy-card *ngSwitchCase="'card'" [title]="w.data.title"></hy-card> 
        <hy-leaflet *ngSwitchCase="'leaflet'" [coords]="w.data.coords"></hy-leaflet> 
      </div>
    </div>

    <ng-template #msg>
      non visible!!!!!!!!
    </ng-template>

  `,
})
export class DynamicDemoComponent implements OnInit {
  @ViewChild('msg') msg!: TemplateRef<any>
  widgets: any[] = [
    {
      type: 'card',
      data: {
        title: 'one',
        icon: 'fa fa-google',
      }
    },
    {
      type: 'toggable',
      data: {
        title: 'two',
        opened: false
      }
    },
    {
      type: 'weather',
      data: {
        city: 'Trieste',
        color: 'green'
      }
    },
    {
      type: 'leaflet',
      data: {
        coords: [43, 13],
        zoom: 5
      }
    }
  ]

  constructor(
    private vcr: ViewContainerRef
  ) { }

  ngOnInit(): void {

    return

    for (let widget of this.widgets) {
      const compo: ComponentRef<any> = this.vcr.createComponent(COMPONENTS[widget.type])
      for (let key in widget.data) {
        // compo.instance[key] = widget.data[key]
        compo.setInput(key, widget.data[key])
      }
    }
  }


  ngAfterViewInit() {
    this.vcr.createEmbeddedView(this.msg)
  }


}

export const COMPONENTS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'toggable': ToggableComponent,
  'weather': WeatherComponent,
  'leaflet': LeafletComponent,
}
