import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { DynamicDemoRoutingModule } from './dynamic-demo-routing.module';
import { DynamicDemoComponent } from './dynamic-demo.component';


@NgModule({
  declarations: [
    DynamicDemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DynamicDemoRoutingModule
  ]
})
export class DynamicDemoModule { }
