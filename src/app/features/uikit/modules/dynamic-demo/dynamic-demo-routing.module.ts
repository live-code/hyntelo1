import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DynamicDemoComponent } from './dynamic-demo.component';

const routes: Routes = [{ path: '', component: DynamicDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicDemoRoutingModule { }
