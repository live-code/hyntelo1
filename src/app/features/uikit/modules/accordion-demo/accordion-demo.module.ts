import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { AccordionDemoRoutingModule } from './accordion-demo-routing.module';
import { AccordionDemoComponent } from './accordion-demo.component';


@NgModule({
  declarations: [
    AccordionDemoComponent
  ],
  imports: [
    CommonModule,
    AccordionDemoRoutingModule,
    SharedModule
  ]
})
export class AccordionDemoModule { }
