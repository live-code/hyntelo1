import { Component } from '@angular/core';

@Component({
  selector: 'hy-accordion-demo',
  template: `
    <hy-row mq="sm">
      <hy-col>ciao</hy-col>
      <hy-col>miao</hy-col>
    </hy-row>

    <hy-toggable title="two">bla bla</hy-toggable>

    <hy-accordion >
      <hy-toggable title="one">
        <hy-leaflet [coords]="[43,13]"></hy-leaflet>
      </hy-toggable>
      <hy-toggable title="two">bla bla</hy-toggable>
      <hy-toggable title="three"x>bla bla</hy-toggable>
    </hy-accordion>
    
  `,
})
export class AccordionDemoComponent  {
}
