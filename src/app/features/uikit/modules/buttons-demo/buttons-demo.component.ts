import { Component, OnInit } from '@angular/core';
import { Country } from '../../../../model/country';

@Component({
  selector: 'hy-buttons-demo',
  template: `
    <p>
      buttons-demo works!
    </p>
    <hy-tabbar
      [items]="countries"
      [active]="activeCountry"
      (tabSelect)="countrySelectHandler($event)"
    ></hy-tabbar>

    <hy-tabbar
      labelField="label"
      [active]="activeCity"
      [items]="activeCountry.cities"
      (tabSelect)="activeCity = $event"
    ></hy-tabbar>
    
    {{activeCity.coords | json}}
    
  `,
})
export class ButtonsDemoComponent  {
  countries: Country[] = [
    {
      id: 1,
      name: 'Italy',
      cities: [
        { id: 1001, label: 'Milan', coords: { lat: 41, lng: 12 } },
        { id: 1002, label: 'Rome', coords: { lat: 42, lng: 13 } } ,
        { id: 1003, label: 'Milan', coords: { lat: 43, lng: 14 } }
      ]

    },
    {
      id: 2,
      name: 'Germany',
      cities: [
        { id: 1001, label: 'Berlino', coords: { lat: 55, lng: 33 } },
      ]
    },
    {
      id: 3,
      name: 'UK',
      cities: [
        { id: 1004, label: 'London', coords: { lat: 66, lng: 22 } },
        { id: 1007, label: 'Leeds', coords: { lat: 66, lng: 22 } },
      ]
    },
  ];


  activeCountry = this.countries[0]
  activeCity = this.activeCountry.cities[0]

  countrySelectHandler(country: Country) {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0]
  }
}
