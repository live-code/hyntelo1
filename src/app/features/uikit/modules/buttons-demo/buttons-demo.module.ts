import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from '../../../../shared/components/card.module';
import { SharedModule } from '../../../../shared/shared.module';

import { ButtonsDemoRoutingModule } from './buttons-demo-routing.module';
import { ButtonsDemoComponent } from './buttons-demo.component';


@NgModule({
  declarations: [
    ButtonsDemoComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    ButtonsDemoRoutingModule,
    SharedModule
  ]
})
export class ButtonsDemoModule { }
