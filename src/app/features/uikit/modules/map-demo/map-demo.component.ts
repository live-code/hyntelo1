import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-map-demo',
  template: `
    <p>
      map-demo works!
    </p>
    
    <hy-leaflet [coords]="coords" [zoom]="zoom"></hy-leaflet>

    <button (click)="coords = [44, 12]">Location 1</button>
    <button (click)="coords = [45, 13]">Location 2</button>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="zoom = zoom - 1">-</button>
    {{render()}}
  `,
  styles: [
  ]
})
export class MapDemoComponent {
  coords: [number, number] = [43, 13];
  zoom = 10;

  render() {
    console.log('render')
  }
}
