import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { MapDemoRoutingModule } from './map-demo-routing.module';
import { MapDemoComponent } from './map-demo.component';


@NgModule({
  declarations: [
    MapDemoComponent
  ],
  imports: [
    CommonModule,
    MapDemoRoutingModule,
    SharedModule
  ]
})
export class MapDemoModule { }
