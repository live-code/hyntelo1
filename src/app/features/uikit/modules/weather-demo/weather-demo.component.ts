import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, fromEvent, map, mergeMap } from 'rxjs';

@Component({
  selector: 'hy-weather-demo',
  template: `
    <input  type="text" placeholder="Search City" [formControl]="input" >
    <hy-weather [city]="city" [color]="myColor"></hy-weather>
    
    <button (click)="myColor = 'purple'">purple</button>
    <button (click)="myColor = 'cyan'">cyan</button>
  `,
})
export class WeatherDemoComponent {
  input = new FormControl('', { nonNullable: true });
  city: string = '';
  myColor = 'red'

  constructor(private http: HttpClient) {
    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
      )
      .subscribe(text => {
        this.city = text;
      })
  }
}
