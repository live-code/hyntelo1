import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

import { WeatherDemoRoutingModule } from './weather-demo-routing.module';
import { WeatherDemoComponent } from './weather-demo.component';


@NgModule({
  declarations: [
    WeatherDemoComponent
  ],
  imports: [
    CommonModule,
    WeatherDemoRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class WeatherDemoModule { }
