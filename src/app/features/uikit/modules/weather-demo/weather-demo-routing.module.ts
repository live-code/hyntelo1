import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeatherDemoComponent } from './weather-demo.component';

const routes: Routes = [{ path: '', component: WeatherDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherDemoRoutingModule { }
