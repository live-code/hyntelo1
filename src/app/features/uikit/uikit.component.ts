import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-uikit',
  template: `
    <h1>UIKIT</h1>
    
    <button routerLink="buttons-demo" routerLinkActive="bg">buttons</button>
    <button routerLink="chart-demo" routerLinkActive="bg">charts</button>
    <button routerLink="panels-demo" routerLinkActive="bg">panels</button>
    <button routerLink="accordion-demo" routerLinkActive="bg">accordion</button>
    <button routerLink="dynamic-demo" routerLinkActive="bg">dynamic</button>
    <button routerLink="map-demo" routerLinkActive="bg">map</button>
    <button routerLink="weather-demo" routerLinkActive="bg">weather</button>
    <br>
    <button routerLink="directive1-demo" routerLinkActive="bg">directive1</button>
    <button routerLink="directive2-demo" routerLinkActive="bg">directive2</button>
    <button routerLink="directive3-demo" routerLinkActive="bg">directive3</button>
    <button routerLink="pipe1-demo" routerLinkActive="bg">Pipes</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .bg { background-color: cyan }
  `]
})
export class UikitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
