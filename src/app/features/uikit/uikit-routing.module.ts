import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitComponent } from './uikit.component';

const routes: Routes = [
  {
    path: '',
    component: UikitComponent,
    children: [
      { path: 'buttons-demo', loadChildren: () => import('./modules/buttons-demo/buttons-demo.module').then(m => m.ButtonsDemoModule) },
      { path: 'chart-demo', loadChildren: () => import('./modules/chart-demo/chart-demo.module').then(m => m.ChartDemoModule) },
      { path: 'panels-demo', loadChildren: () => import('./modules/panels-demo/panels-demo.module').then(m => m.PanelsDemoModule) },
      { path: 'accordion-demo', loadChildren: () => import('./modules/accordion-demo/accordion-demo.module').then(m => m.AccordionDemoModule) },
      { path: 'map-demo', loadChildren: () => import('./modules/map-demo/map-demo.module').then(m => m.MapDemoModule) },
      { path: 'dynamic-demo', loadChildren: () => import('./modules/dynamic-demo/dynamic-demo.module').then(m => m.DynamicDemoModule) },
      { path: 'weather-demo', loadChildren: () => import('./modules/weather-demo/weather-demo.module').then(m => m.WeatherDemoModule) },
      { path: 'directive1-demo', loadChildren: () => import('./modules/directive1-demo/directive1-demo.module').then(m => m.Directive1DemoModule) },
      { path: 'directive2-demo', loadChildren: () => import('./modules/directive2-demo/directive2-demo.module').then(m => m.Directive2DemoModule) },
      { path: 'directive3-demo', loadChildren: () => import('./modules/directive3-demo/directive3-demo.module').then(m => m.Directive3DemoModule) },
      { path: 'pipe1-demo', loadChildren: () => import('./modules/pipe1-demo/pipe1-demo.module').then(m => m.Pipe1DemoModule) },
      { path: '', redirectTo: 'buttons-demo', pathMatch: 'full'}
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitRoutingModule { }
