import { Component, OnInit } from '@angular/core';
import { getClientId, getClientIdWithServerSideCall } from '../../clients-routing.module';

@Component({
  selector: 'hy-roles',
  template: `
    <h3>ROLES (CHILD) </h3>
    <h2>CLIENT ID: {{clientId}} <-- real (got from server)</h2>
  `,
  styles: [
  ]
})
export class RolesComponent  {
  clientId: number | undefined;

  constructor() {
    getClientIdWithServerSideCall()
      .subscribe(id => this.clientId = id)
  }

}
