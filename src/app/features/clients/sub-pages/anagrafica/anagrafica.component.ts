import { Component, OnInit } from '@angular/core';
import { getClientId } from '../../clients-routing.module';

@Component({
  selector: 'hy-anagrafica',
  template: `
    <h3>ANAGRAFICA (CHILD) </h3>
    <h2>CLIENT ID: {{clientId$ | async}}</h2>
  `,
})
export class AnagraficaComponent  {
  clientId$ = getClientId();
}
