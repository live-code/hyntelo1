import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable } from 'rxjs';
import { getClientId } from '../../clients-routing.module';

@Component({
  selector: 'hy-another',
  template: `
    <h3>ANOTHER PAGE (CHILD) - route with resolver </h3>
    <h2>CLIENT ID: {{(clientId$ | async)}}  <-- real (got from server)</h2>
  `,
})
export class AnotherComponent implements OnInit {
  clientId$: Observable<number> = this.activatedRoute.data
    .pipe(
      map(obj => obj['id'])
    )

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ id }) => {
      console.log('id', id)
    })

  }

}
