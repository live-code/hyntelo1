import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { map, Observable, of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ClientIdResolver implements Resolve<number> {

  constructor(private http: HttpClient) {
  }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<number> {
    // return of(123);
    return this.http.get<any>(`http://localhost:3000/clients?key=${route.paramMap.get('clientName')}`)
      .pipe(
        map((res: any) => res[0].id)
      )
  }
}
