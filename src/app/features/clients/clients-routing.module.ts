import { HttpClient } from '@angular/common/http';
import { inject, NgModule } from '@angular/core';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';
import { map, switchMap } from 'rxjs';
import { ClientIdResolver } from './clientId.resolver';
import { ClientsComponent } from './clients.component';
import { AnagraficaComponent } from './sub-pages/anagrafica/anagrafica.component';
import { AnotherComponent } from './sub-pages/another/another.component';
import { RolesComponent } from './sub-pages/roles/roles.component';

const routes: Routes = [
  {
    path: ':clientName', component: ClientsComponent,
    children: [
      { path: 'anagrafica', component: AnagraficaComponent },
      { path: 'roles', component: RolesComponent },
      {
        path: 'another', component: AnotherComponent,
        resolve: {
          id: ClientIdResolver
        }
      },
    ],
  },
  { path: '', redirectTo: 'ABC/anagrafica', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }

// ====== SOLUTION TO GET the CLIENT ID from URLs like these:

// SOLUTION 1
// This only works on parent component '/clients'
export function getClientId_SOLUTION1() {
  return inject(ActivatedRoute).paramMap
    .pipe(
      map((params) => params.get('clientName')),
      // NOTE: we should get the id by using HttpClient =>  switchMap ( get the ID .. see next example)
      // but I simply Map it to a fake id
      map(clientName => 'FAKE_ID_12')
    )
}
// SOLUTION 2
// This only works on children component '/clients/123/anagrafica'
export function getClientId_SOLUTION2() {
  return inject(ActivatedRoute).parent?.paramMap
    .pipe(
      map((params) => params.get('clientName')),
      // NOTE: we should get the id by using HttpClient =>  switchMap ( get the ID .. see next example)
      // but I simply Map it to a fake id
      map(clientName => 'FAKE_ID_123')
    )
}


// SOLUTION 3
// This works on parent and children
// NOTE: to work everywhere we need to set following code in `app.routing.ts`
// RouterModule.forRoot(APP_ROUTES, { paramsInheritanceStrategy: 'always'})

export function getClientId() {
  return inject(ActivatedRoute).paramMap
    .pipe(
      // switchMap((params) => inject(HttpClient).get(`...${params.get('clientName')}`)
      map((params) => params.get('clientName')),
      // NOTE: we should get the id by using HttpClient =>  switchMap ( get the ID .. see next example)
      // but I simply Map it to a fake id
      map(clientName => 'FAKE_ID_1234')
    )
}



// SOLUTION WITH SERVER SIDE CALL
// This works on parent and children
// NOTE: to work everywhere we need to set following code in `app.routing.ts`
// RouterModule.forRoot(APP_ROUTES, { paramsInheritanceStrategy: 'always'})

export function getClientIdWithServerSideCall() {
  return inject(ActivatedRoute).paramMap
    .pipe(
      switchMap((params) => inject(HttpClient)
          .get(`http://localhost:3000/clients?key=${ params.get('clientName')}`)
      ),
      map((res: any) => res[0].id)
      // map((params) => params.get('clientName'))
    )
}


// SOLUTION 4: with resolver
// see: clients-resolver.ts and clients-routing.module.ts

// SOLUTION 5: services
/*
• creare un service `ClientIdService`
• si inietta `activateRoute`, si recupera i params e si espone come observable)
• si definisce il provider nel parent component (ClientComponent) - in modo che possa recuperare i parametri
  { provider: ClientIdService }
• nei componenti si inietta il service e si usa l'observable esposto dal Service che permette di recuperare i params

  constructor(private clientId: ClientIdService) {
    clientId.subscribe(...)
  }
 */


// trick

/*
var params = [];
var route = router.routerState.snapshot.root;
do {
  params.push(route.params);
  route = route.firstChild;
} while(route);
*/
