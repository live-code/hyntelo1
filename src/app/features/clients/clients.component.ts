import { Component, OnInit } from '@angular/core';
import { getClientId } from './clients-routing.module';

@Component({
  selector: 'hy-clients',
  template: `
    
    <h1>CLIENT component (parent)</h1>
    <!--<h2>CLIENT NAME:{{clientId$ | async}}</h2>-->
    <hr>
    <button routerLink="anagrafica" routerLinkActive="bg">anagrafica</button>
    <button routerLink="roles" routerLinkActive="bg">roles</button>
    <button routerLink="another" routerLinkActive="bg">another</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .bg { background-color: cyan }
  `]
})
export class ClientsComponent {
  // clientId$ = getClientId();
  constructor() {
  }
}
