import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsComponent } from './clients.component';
import { AnagraficaComponent } from './sub-pages/anagrafica/anagrafica.component';
import { RolesComponent } from './sub-pages/roles/roles.component';
import { AnotherComponent } from './sub-pages/another/another.component';


@NgModule({
  declarations: [
    ClientsComponent,
    AnagraficaComponent,
    RolesComponent,
    AnotherComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule
  ]
})
export class ClientsModule { }
