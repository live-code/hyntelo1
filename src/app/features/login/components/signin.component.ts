import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'hy-signin',
  template: `
    <h1>Sign In</h1>
    <input type="text">
    <input type="text">
    <button (click)="authService.login()">Login</button>

  `,
  styles: [
  ]
})
export class SigninComponent implements OnInit {

  constructor(public authService: AuthService) {
  }
  ngOnInit(): void {
  }

}
