import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from './login.component';
import { RegistrationComponent } from './components/registration.component';
import { LostpassComponent } from './components/lostpass.component';
import { SigninComponent } from './components/signin.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'lostpass', component: LostpassComponent },
      { path: 'signin', component: SigninComponent },
      { path: 'registration', component: RegistrationComponent },
      { path: '', redirectTo: 'signin', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    LostpassComponent,
    SigninComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    SharedModule,
  ]
})
export class LoginModule { }
