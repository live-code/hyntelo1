import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hy-login',
  template: `
    <h1>Login</h1>
    
    <hr>
    <router-outlet></router-outlet>
    <hr>
    <button routerLink="lostpass" routerLinkActive="bg">lostpass</button>
    <button routerLink="signin" routerLinkActive="bg">signin</button>
    <button routerLink="registration" routerLinkActive="bg">registration</button>
  `,
  styles: [`
    .bg { background-color: cyan}
  `]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
