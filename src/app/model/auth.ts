export type Role = 'admin' | 'moderator';

export interface Auth {
  displayName: string;
  token: string;
  role: Role;
}
