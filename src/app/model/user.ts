export type User = {
  id: number;
  name: string;
  address: string;
}

// TS utility types
export type UserFormData = Omit<User,  'id'>



function doSomething(u: User) {

}

function doSomethingElse(u: UserFormData) {
}
