import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from './features/contacts/contacts.component';
import { HomeComponent } from './features/home/home.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(res => res.CatalogModule)},
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  { path: 'clients', loadChildren: () => import('./features/clients/clients.module').then(m => m.ClientsModule) },
  { path: 'contacts', component: ContactsComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
]
@NgModule({
  imports: [
    // RouterModule.forRoot(APP_ROUTES),
    RouterModule.forRoot(APP_ROUTES, { paramsInheritanceStrategy: 'always'})
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {

}
