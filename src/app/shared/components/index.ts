import { CardComponent } from './card.component';
import { TabbarComponent } from './tabbar.component';

export const COMPONENTS = [
  CardComponent,
  TabbarComponent,
]
