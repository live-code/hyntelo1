import { Component, EventEmitter, Input, OnInit, Optional, Output } from '@angular/core';
import { AccordionComponent } from './accordion.component';

@Component({
  selector: 'hy-toggable',
  template: `
    <div>
      <div class="card">
        <div
          class="card-header bg-dark text-white"
          (click)="toggle()"
        >
          <div class="d-flex justify-content-between align-items-center">
            <div>{{title}}</div>
          </div>
        </div>
        <div class="card-body" *ngIf="opened">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class ToggableComponent {
  @Input() title = 'Hello Toggable'
  @Input() opened = true;
  // Trick for 2-way binding
  @Output() openedChange = new EventEmitter();

  toggle() {
    this.opened = !this.opened;
    this.openedChange.emit(this.opened)
  }

 /* constructor(
    @Optional() private accordion: AccordionComponent
  ) {
  }*/
}


