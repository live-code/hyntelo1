import { Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ToggableComponent } from './toggable.component';

@Component({
  selector: 'hy-accordion',
  template: `
    <div style="border: 3px solid blue; padding: 10px; margin: 10px">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent  {
  @ContentChildren(ToggableComponent) panels!: QueryList<ToggableComponent>;

  ngAfterContentInit() {
    this.panels.toArray().forEach(panel => {
      panel.opened = false
      panel.openedChange.subscribe(() => {
        this.closeAll();
        panel.opened = true;
      })
    })
    this.panels.toArray()[0].opened = true;
  }

  closeAll() {
    this.panels.toArray().forEach(panel => {
      panel.opened = false;
    })
  }

}
