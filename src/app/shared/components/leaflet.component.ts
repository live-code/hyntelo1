import { ChangeDetectionStrategy, Component, ElementRef, Input, NgZone, SimpleChanges, ViewChild } from '@angular/core';
import * as L from 'leaflet';
import { LatLngExpression } from 'leaflet';

@Component({
  selector: 'hy-leaflet',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { height: 180px; width: 100%}
  `]
})
export class LeafletComponent {
  @ViewChild('host', { static: true } ) host!: ElementRef<HTMLDivElement>
  @Input() coords: LatLngExpression = [43, 13]
  @Input() zoom: number = 5;
  map!: L.Map;

  initMap() {
    this.ngZone.runOutsideAngular(() => {
      this.map = L.map(this.host.nativeElement).setView(this.coords, 13);
      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
      }).addTo(this.map);
    })
  }

  constructor(private ngZone: NgZone) {}

  ngOnChanges(changes: SimpleChanges) {
    const { coords, zoom } = changes;

    if (!this.map) {
      this.initMap();
    }

    if (coords && coords.currentValue) {
      this.map.setView(this.coords)
    }

    if (zoom && zoom.currentValue) {
      this.map.setZoom(zoom.currentValue)
    }

  }

}
