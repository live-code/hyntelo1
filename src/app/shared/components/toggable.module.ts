import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToggableComponent } from './toggable.component';



@NgModule({
  declarations: [ToggableComponent],
  exports: [ToggableComponent],
  imports: [
    CommonModule
  ]
})
export class ToggableModule { }
