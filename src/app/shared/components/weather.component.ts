import { HttpClient } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'hy-weather',
  template: `
    <h1 [style.color]="color">Weather</h1>
    <div *ngIf="meteo">
      <pre [style.color]="color">{{meteo.main.temp}}°</pre>
      <img [src]="'http://openweathermap.org/img/w/' + meteo.weather[0].icon + '.png'" alt="">
    </div>
  `,
})
export class WeatherComponent implements OnChanges {
  meteo: Meteo | undefined;

  /*  @Input() set city(val: string) {
      if (val) {
        this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${val}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .subscribe(res => {
            this.meteo = res
          })
      }
    }*/
  @Input() city: string | undefined;
  @Input() color: string | undefined;

  constructor(private http: HttpClient) {}

  ngOnChanges(changes: SimpleChanges) {
    const {city, color } = changes;

    if (city && city.currentValue) {
      this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = res
        })
    }
    if (color) {

    }
  }


}
