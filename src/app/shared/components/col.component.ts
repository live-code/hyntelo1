import { Component, HostBinding, OnInit, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'hy-col',
  template: `
      <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() class = 'col-' + this.parent.mq;

  constructor(@Optional() private parent: RowComponent) {
    if(!parent) {
      throw new Error('col should be used inside an accordtion')
    }
    console.log(parent.mq)
  }

}
