import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * This is a generic tabbar
 */
@Component({
  selector: 'hy-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li *ngFor="let tab of items" class="nav-item"
          (click)="tabSelect.emit(tab)">
        <a class="nav-link"
           [ngClass]="{'active': tab.id === active?.id}">
          {{tab[labelField]}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent<T extends { id: number, [key:string]: any}>  {
  /**
   * List of items to show
   */
  @Input() items: T[] = [];
  /**
   * Actove element
   */
  @Input() active: T | undefined;
  @Input() labelField = 'name';
  @Output() tabSelect = new EventEmitter<T>();
}
