import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'hy-card',
  template: `
    <div>
      <div class="card">
        <div
          class="card-header"
          (click)="opened = !opened"
        >
          <div class="d-flex justify-content-between align-items-center">
            <div>{{title}}</div>
            <i 
              *ngIf="icon"
              (click)="doAction($event)"
              [class]="icon"></i>
          </div>
        </div>
        <div class="card-body" *ngIf="opened">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title = 'Hello Card'
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter();
  opened = true;

  doAction(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }
}


