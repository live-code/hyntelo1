import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'usersSort'
})
export class UsersSortPipe implements PipeTransform {

  transform(users: User[] | null, order: 'ASC' | 'DESC' = 'ASC'): User[]{
    if (users) {
      const result = users.sort((a, b) => a.name.localeCompare(b.name));
      return order === 'ASC' ? result : result.reverse();
    }
    return [];

  }

}
