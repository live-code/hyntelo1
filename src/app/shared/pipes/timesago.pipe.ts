import { Pipe, PipeTransform } from '@angular/core';
import { formatDistance } from 'date-fns';

@Pipe({
  name: 'timesago'
})
export class TimesagoPipe implements PipeTransform {

  transform(date: Date | number, addSuffix: boolean = true): unknown {
    return formatDistance(date, new Date(), { addSuffix })
  }

}
