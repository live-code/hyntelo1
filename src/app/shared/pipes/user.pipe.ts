import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { map, Observable, share } from 'rxjs';
import { User } from '../../model/user';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {

  transform(id: number): Observable<User> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
  }

  constructor(private http: HttpClient) {
  }

}
