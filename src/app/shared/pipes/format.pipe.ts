import { Pipe, PipeTransform } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Pipe({
  name: 'format',
  pure: false
})
export class FormatPipe implements PipeTransform {

  transform(value: number, formatType: 'gb' | 'mb'): string {
    switch(formatType) {
      case 'mb':
        return `${value * 1000000}mb`;
      case 'gb':
        return `${value * 1000000}gb`;
      default:
        return '';
    }
  }

  constructor(private auth: AuthService) {
  }
}
