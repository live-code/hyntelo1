import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'usersSearch'
})
export class UsersSearchPipe implements PipeTransform {

  transform(users: User[] | null, textToFind: string): User[] {
    if (users) {
      return users.filter(u => {
        return u.name.toLowerCase().includes(textToFind.toLowerCase())
      });
    }
    return []
  }
}
