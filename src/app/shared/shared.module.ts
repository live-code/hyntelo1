import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { CardModule } from './components/card.module';
import { TabbarComponent } from './components/tabbar.component';
import { ToggableModule } from './components/toggable.module';
import { WeatherComponent } from './components/weather.component';
import { AccordionComponent } from './components/accordion.component';
import { LeafletComponent } from './components/leaflet.component';
import { RowComponent } from './components/row.component';
import { ColComponent } from './components/col.component';
import { BgDirective } from './directives/bg.directive';
import { BorderDirective } from './directives/border.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { XhrDirective } from './directives/xhr.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { LoeaderDirective } from './directives/loeader.directive';
import { MapDirective } from './directives/map.directive';
import { FormatPipe } from './pipes/format.pipe';
import { TimesagoPipe } from './pipes/timesago.pipe';
import { UserPipe } from './pipes/user.pipe';
import { UsersSortPipe } from './pipes/users-sort.pipe';
import { UsersSearchPipe } from './pipes/users-search.pipe';


@NgModule({
  declarations: [
    TabbarComponent,
    WeatherComponent,
    AccordionComponent,
    LeafletComponent,
    RowComponent,
    ColComponent,
    BgDirective,
    BorderDirective,
    XhrDirective,
    UrlDirective,
    StopPropagationDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    LoeaderDirective,
    MapDirective,
    FormatPipe,
    TimesagoPipe,
    UserPipe,
    UsersSortPipe,
    UsersSearchPipe

  ],
  imports: [
    CardModule,
    ToggableModule,
    CommonModule
  ],
  exports: [
    CardModule,
    ToggableModule,
    TabbarComponent,
    WeatherComponent,
    AccordionComponent,
    LeafletComponent,
    RowComponent,
    ColComponent,
    BgDirective,
    BorderDirective,
    XhrDirective,
    UrlDirective,
    StopPropagationDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    LoeaderDirective,
    MapDirective,
    FormatPipe,
    TimesagoPipe,
    UserPipe,
    UsersSortPipe,
    UsersSearchPipe

  ]
})
export class SharedModule { }
