import { Directive, HostListener, Input, ViewContainerRef } from '@angular/core';
import { LeafletComponent } from '../components/leaflet.component';

@Directive({
  selector: '[hyMap]'
})
export class MapDirective {
  @Input('hyMap') coords: [number, number] | undefined;

  @HostListener('mouseover')
  mouseOver() {
    const compo = this.vcr.createComponent(LeafletComponent);
    compo.setInput('coords', this.coords)
  }

  @HostListener('mouseout')
  mouseOut() {
    this.vcr.clear()
  }

  constructor(private vcr: ViewContainerRef) {
  }

}
