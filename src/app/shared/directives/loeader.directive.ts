import { ComponentRef, Directive, Input, Type, ViewContainerRef } from '@angular/core';
import { COMPONENTS } from '../../features/uikit/modules/dynamic-demo/dynamic-demo.component';
import { CardComponent } from '../components/card.component';
import { LeafletComponent } from '../components/leaflet.component';
import { ToggableComponent } from '../components/toggable.component';
import { WeatherComponent } from '../components/weather.component';

@Directive({
  selector: '[hyLoeader]'
})
export class LoeaderDirective {
  @Input('hyLoeader') config: any

  constructor(
    private vcr: ViewContainerRef
  ) { }

  ngOnChanges() {
    this.vcr.clear();
    const compo: ComponentRef<any> = this.vcr.createComponent(WIDGETS[this.config.type])
    for (let key in this.config.data) {
      compo.setInput(key, this.config.data[key])
    }
  }

}

export const WIDGETS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'toggable': ToggableComponent,
  'weather': WeatherComponent,
  'leaflet': LeafletComponent,
}
