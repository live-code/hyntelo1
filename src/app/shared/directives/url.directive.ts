import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[hyUrl]'
})
export class UrlDirective {
  @Input() hyUrl: string = ''

  @HostListener('click')
  openUrl() {
    window.open(this.hyUrl)
  }
}
