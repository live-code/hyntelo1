import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[hyStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  doSomething(e: MouseEvent) {
    e.stopPropagation()
  }

}
