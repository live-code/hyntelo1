import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[hyBg]'
})
export class BgDirective {
  @HostBinding('style.backgroundColor') get bg() {
    return this.bgColor;
  }
  @Input('hyBg') bgColor = 'purple'
}

