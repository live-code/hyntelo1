import { HttpClient } from '@angular/common/http';
import { Directive, EventEmitter, Input, Output } from '@angular/core';

@Directive({
  selector: '[hyXhr]'
})
export class XhrDirective {
  @Output() success = new EventEmitter<any>();

  @Input() set hyXhr(url: string) {
    this.http.get(url)
      .subscribe(res => {
        this.success.emit(res)
      })
  }
  constructor(private http: HttpClient) { }

}
