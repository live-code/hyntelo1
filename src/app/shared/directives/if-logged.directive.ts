import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { map } from 'rxjs';
import { AuthService } from '../../core/auth/auth.service';

@Directive({
  selector: '[hyIfLogged]'
})
export class IfLoggedDirective {
  constructor(
    private authService: AuthService,
    private templateRef: TemplateRef<any>,
    private vcr: ViewContainerRef
  ) {
    this.authService.isLogged$
      .subscribe(isLogged => {
        if (isLogged) {
          vcr.createEmbeddedView(templateRef)
        } else {
          vcr.clear()
        }
      })

  }

}
