import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[hyBorder]'
})
export class BorderDirective {
  @Input() hyBorder = 'solid'
  @Input() color = 'red'

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {}

  ngOnChanges() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'border',
      `2px ${this.hyBorder} ${this.color}`
    )
    /*
      this.el.nativeElement.style.border =
          `2px ${this.hyBorder} ${this.color}`
      */
  }

}
