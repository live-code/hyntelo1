import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { Role } from '../../model/auth';

@Directive({
  selector: '[hyIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() hyIfRoleIs: Role | Role[] | undefined;

  constructor(
    private authService: AuthService,
    private templateRef: TemplateRef<any>,
    private vcr: ViewContainerRef
  ) {}

  ngOnInit() {
    this.authService.role$
      .subscribe(userRole => {

        if (typeof this.hyIfRoleIs === 'string') {
          if (userRole === this.hyIfRoleIs) {
            this.vcr.createEmbeddedView(this.templateRef)
          } else {
            this.vcr.clear()
          }
        }

        if (typeof this.hyIfRoleIs === 'object') {
          const isValid = this.hyIfRoleIs.some(item => item === userRole)
          if (isValid) {
            this.vcr.createEmbeddedView(this.templateRef)
          } else {
            this.vcr.clear()
          }
        }
      })
  }

}
