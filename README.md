# TOPICS

### TOPICS (8 sep):

* Attribute Directives <div bg="red"> <div routerLink="/contacts">
  * HostBinding <div class="alert-custom">
  * @Input
  * HostListener
  * ElementRef
  * Reactive
* Structural Directives <div *ifRoleIs="admin">...</div>
  * TemplateRef
  * ViewContainerRef
  * Protect DOM (signin / roles)
* Custom Pipe  
  * Pure {{myDate | timesago}}
  * Impure
  * Sync
  * Async  (myId | user)
  * ngFor   <div *ngFor="let user of users | filterByGender: ....  | sortBy: .... | search: ....>



### TOPICs (7 sep)
* UIKIT components
  * Panel
  * Toggable
  * Accordion
  * Leaflet
  * Weather
  * TabBar
  * Grid and Col

### TOPICs (6 sep)
* Component Based Approach
* Change Detection Strategies
* Dependency Injection (intro)

### TOPIC (5 sep)
* ngModules
  * Features (routed modules)
  * Routing Modules
  * Core Module
  * Shared Module
  * Widget Modules
  * Domain MOdules
* Router
  * Lazy Loading
  * Nested



# QUESTIONS:
- ROUTER: URL con primo segmento dinamico /fabiobiondi/users or /ciccio/users
- skinning datepicker material (alternative a ngdeep o !important)
